package com.oehm4.basics.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.oehm4.basics.dto.AccountDTO;
import com.oehm4.basics.utils.SessionFactoryUtil;

public class HqlAccountDAO {

	/*
	 Hql delete and update operations
	 */
	public List<AccountDTO> getAccountDetails() {
		/*
		Configuration configuration = new Configuration();
		configuration.configure();
		//configuration.addAnnotatedClass(AccountDTO.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="from AccountDTO";
		Query query = session.createQuery(hql);
		List<AccountDTO> list = query.list();
		return list;
		
		*/
		//cache implementation
		
		Session session = SessionFactoryUtil.createSessionFactory().openSession();
		String hql="from AccountDTO";
		Query query = session.createQuery(hql);
		query.setCacheable(true);
		List<AccountDTO> list = query.list();
		return list;

	}
	
	public AccountDTO getAccountDetailsByAccountNumber(String accountNumber) {
		Configuration configuration = new Configuration();
		configuration.configure();
		//configuration.addAnnotatedClass(AccountDTO.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="from AccountDTO where accountNumber=:aNumber";
		Query query = session.createQuery(hql);
		query.setParameter("aNumber", accountNumber );
		AccountDTO uniqueResult = (AccountDTO) query.uniqueResult();
		return uniqueResult;
	}
	
	public void updateAddressByAccountNumber(String accountNumber,String newAddress) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		String hql="update AccountDTO set address=:newAddress where accountNumber=:accountNumber";
		Query query = session.createQuery(hql);
		query.setParameter("newAddress", newAddress);
		query.setParameter("accountNumber", accountNumber);
		int updateRows = query.executeUpdate();
		transaction.commit();
		if(updateRows == 0) {
			System.out.println("Update Operation Failed");
			return;
		}
		System.out.println("Update Operation successfull");
	}
	
	public void deleteById(Long id) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		String hql="delete AccountDTO where id=:id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		int updateRows = query.executeUpdate();
		transaction.commit();
		if(updateRows == 0) {
			System.out.println("Delete Operation Failed");
			return;
		}
		System.out.println("Delete Operation successfull");
	}
}
