package com.oehm4.basics.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.oehm4.basics.dto.Apps;
import com.oehm4.basics.dto.Brand;
import com.oehm4.basics.dto.Movie;
import com.oehm4.basics.dto.Team;

public class AssociationsDAO {
/*
 one to one associations
 */
	public void saveTeamDetails(Team team) {
		Configuration configuration = new Configuration();
		//configuration.configure("config.xml");
		configuration.configure();
	 	SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(team);
		transaction.commit();	
	}
	/*
	 one to many associations
	 */
	public void saveBrandDetails(Brand brand) {
		Configuration configuration = new Configuration();
		//configuration.configure("config.xml");
		configuration.configure();
	 	SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(brand);
		transaction.commit();	
	}
	
	//many to one associations
	
	public void saveAppsDetails(Apps apps) {
		Configuration configuration = new Configuration();
		//configuration.configure("config.xml");
		configuration.configure();

	 	SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(apps);
		transaction.commit();	
	}
	
	
	//many to many associations
	
	public void saveMovieDetails(Movie movie) {
		Configuration configuration = new Configuration();
		//configuration.configure("config.xml");
		configuration.configure();
	 	SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(movie);
		transaction.commit();	
	}

}


