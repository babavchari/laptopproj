package com.oehm4.basics.dto;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name = "team")
public class Team implements Serializable{
	/*
	 one to one associations
	 */

	@Id
	@GenericGenerator(name = "team_auto", strategy = "increment")
	@GeneratedValue(generator = "team_auto")
	@Column(name = "id")
	private Long id;
	@Column(name = "name")
	private String name;
	@Column(name = "jersey_color")
	private String jerseyColor;
	@Column(name = "type")
	private String type;
	@Column(name = "size")
	private Long size;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "captain_id")
	private Captian captain;
	
	public Team() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJerseyColor() {
		return jerseyColor;
	}

	public void setJerseyColor(String jerseyColor) {
		this.jerseyColor = jerseyColor;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public Captian getCaptain() {
		return captain;
	}

	public void setCaptain(Captian captain) {
		this.captain = captain;
	}

	@Override
	public String toString() {
		return "Team [id=" + id + ", name=" + name + ", jerseyColor=" + jerseyColor + ", type=" + type + ", size="
				+ size + ", captain=" + captain + "]";
	}
	
	
}
