package com.oehm4.basics.dto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;



/*
 one to many associations
 
 */
@Entity
@Table(name = "brand")
public class Brand implements Serializable{

	@Id
	@GenericGenerator(name = "brand_auto", strategy = "increment")
	@GeneratedValue(generator = "brand_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "since")
	private String since;
	
	@Column(name = "ambassdor")
	private String ambassdor;
	
	@Column(name = "origin")
	private String origin;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "brand_id")
	private List<Model> model;

	public Brand() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSince() {
		return since;
	}

	public void setSince(String since) {
		this.since = since;
	}

	public String getAmbassdor() {
		return ambassdor;
	}

	public void setAmbassdor(String ambassdor) {
		this.ambassdor = ambassdor;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public List<Model> getModel() {
		return model;
	}

	public void setModel(List<Model> model) {
		this.model = model;
	}

	
	
}
