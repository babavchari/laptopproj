package com.oehm4.basics.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "operating_system")
public class System implements Serializable{
	
	@Id
	@GenericGenerator(name = "os_auto", strategy = "increment")
	@GeneratedValue(generator = "os_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "os_type")
	private String osType;
	
	@Column(name = "version")
	private Double version;
	
	@Column(name = "name")
	private String name;
	
	public System() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}

	public Double getVersion() {
		return version;
	}

	public void setVersion(Double version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
