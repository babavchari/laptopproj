package com.oehm4.basics.dto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "movie")
public class Movie implements Serializable{

	@Id
	@GenericGenerator(name = "apps_auto", strategy = "increment")
	@GeneratedValue(generator = "apps_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "movie_name")
	private String movieName;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "m_a_common",
	joinColumns = {	@JoinColumn(name = "m_id")	},
	inverseJoinColumns = {@JoinColumn(name = "a_id")}
	)
	private List<Actor> actors;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public List<Actor> getActors() {
		return actors;
	}

	public void setActors(List<Actor> actors) {
		this.actors = actors;
	}
	
	
	
}
