package com.oehm4.basics.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name = "captain")
public class Captian implements Serializable{
/*
  one to one associations
 
 */
	@Id
	@GenericGenerator(name = "captain_auto", strategy = "increment")
	@GeneratedValue(generator = "captain_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "age")
	private Long age;
	
	@Column(name = "total_experience")
	private Long totalExperience;
	
	@Column(name = "skills")
	private String skills;
	
	public Captian() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAge() {
		return age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public Long getTotalExperience() {
		return totalExperience;
	}

	public void setTotalExperience(Long totalExperience) {
		this.totalExperience = totalExperience;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	@Override
	public String toString() {
		return "Captain [id=" + id + ", name=" + name + ", age=" + age + ", totalExperience=" + totalExperience
				+ ", skills=" + skills + "]";
	}
	
}
